;; feral cats
;;
;; ref. https://doi.org/10.1016/j.ecolmodel.2016.06.014


breed [ cats cat ]

patches-own [
  capacity ;; number of cats a colony can carry without  lack of resources
  colony-index ;; an identifier for the colony
  social-classes-number ;; how many cat classes there is in this colony
]

cats-own [
  sex ;; F, M
  age ;; int : in days
  sexual-maturity-state ;; bool
  sexual-maturity-date ;; age in days after which the cat is considered sexually mature (fixed at birth)
  reproductive-control ;; none | spayed/neutered | vasectomy/hysterectomy
  dominance-score ;; float ∈ [0, 1], 1 is the score of the most dominant male (that is to say, the oldest male in our model)
  social-class ;; int, the categorical number of the female social class
  pregnancy-state ;; none | estrus | diestrus | pregnant | pseudo-pregnant | nurse
  pregnancy-date ;; days of the diestrus cycle or the pregnancy state
  copulation-count ;; int, number of copulation
  colony ;; int: colony the cat belongs to
]

globals [
  JUVENILE_AGE_BEGIN
  ADULT_AGE_BEGIN
  CAT_LONGEVITY
  BASELINE_SURVIVAL_RATE
  ESTRUS_MAX_DURATION
  DIESTRUS_DURATION
  PREGNANCY_DURATION
  PSEUDOPREGNANCY_DURATION
  NURSE_DURATION
  day ;; ranges from 0 to 365
  year ;; up to 6 years of simulation, in the original paper
  migrations ;; list [ from-colony to-colony ], this day.
]

to set-parameters
  set JUVENILE_AGE_BEGIN 42 ;; days, Kitten age intervalle end
  set ADULT_AGE_BEGIN 180 ;; days, Juvenile age intervalle end
  set CAT_LONGEVITY 15 * 365 ;; 15 years seems a respectable cat longevity
  set ESTRUS_MAX_DURATION 8 ;; days, maximum duration of estrus cycle
  set DIESTRUS_DURATION 5 ;; days, diastrus period end before the next estrus
  set PREGNANCY_DURATION 25 ;; days, pregnancy duration end
  set PSEUDOPREGNANCY_DURATION 45 ;; days, pseudopregnancy duration end
  set NURSE_DURATION 42 ;; days, weaning kittends end
  set BASELINE_SURVIVAL_RATE 0.9991

  ;; Initialize migrations reporter
  set migrations []
end

;;
;; Cat survival rates
;;

to-report cat-survival-rate
  let general-rate 0.9991
  let kittens-rate 0.99233
  let juveniles-rate 0.99233
  let non-neutered-adult-males-rate 0.99804
  let unspayed-adult-females-rate 0.99904
  let spayed-neutered-adults-rate 0.99905
  if is-kitten
  [ report kittens-rate ]
  if is-juvenile
  [ report juveniles-rate ]
  ifelse sex = "F"
  [ ifelse reproductive-control = "none"
    [ report unspayed-adult-females-rate ]
    [ report spayed-neutered-adults-rate ]
  ]
  [ ifelse reproductive-control = "none"
    [ report non-neutered-adult-males-rate ]
    [ report spayed-neutered-adults-rate ]
  ]
end

to-report neighbor-cats [ cat-colony ]
  report cats with [ colony = cat-colony ]
end

to-report colony-size
 report count neighbor-cats colony
end

to-report daily-cat-survival-rate
  report BASELINE_SURVIVAL_RATE - ((BASELINE_SURVIVAL_RATE - cat-survival-rate) * colony-size) / (colony-capacity)
  ;; Prob_S = S_0 - ((S_0 - S_K) P) / S_K
end

to-report colony-capacity
  let K 0
  ask patch-here [
    set K capacity
  ]
  if K = 0
  [ error "Error: capacity is null, am I on a colony patch?" ]
  report K
end

to-report colony-social-classes
  ask patch-here [
    set social-classes-number ceiling (capacity / 14) ;; number of female social classes in the colony
  ]
  report social-classes-number
end

;; A random value within a range (int)
to-report random-within [ range-min range-max ]
  report range-min + (random (range-max - range-min + 1))
end

;; Get a number of days given a number of months (Oversimplification)
;; considering an abritrary month duration of 30 days
to-report month-to-days [ month ]
  report month * 30
end

to-report cat-sexual-maturity-date
  if sex = "F"
  [
    let start-day month-to-days 10
    let end-day month-to-days 12
    let random-threshold (random-within start-day end-day)
    report random-threshold
  ]
  if sex = "M"
  [
    let start-day month-to-days 12
    let end-day month-to-days 14
    let random-threshold random-within start-day end-day
    report random-threshold
  ]
  error "Error: to-report in-cat-sexual-maturity-date: cat sex is not in { F, M }"
end

to-report is-kitten
  report age < JUVENILE_AGE_BEGIN
end

to-report is-juvenile
  report age < ADULT_AGE_BEGIN and age > JUVENILE_AGE_BEGIN
end

;;
;; Setup
;;

to setup
  clear-all
  setup-colonies
  set-parameters
  initial-population
  reset-ticks
end

to setup-colonies
  ifelse fixed-colony-position
  [
    if nb-colonies != 4
    [ print "Warning: when using fixed-colony-position, the number of colony is set to 4."
      set nb-colonies 4
    ]
    let colony-positions [ [-5 5] [5 5] [0 0] [0 -7] ]
    let index 1
    foreach colony-positions [ coor ->
      ask patch (first coor) (last coor) [
        set colony-index index
        set pcolor white
        set capacity 10
        set index index + 1
      ]
    ]
    ask patch 0 -7 [
      set capacity 40
    ]
  ]
  [
  with-local-randomness [
    random-seed 6 ;; ensure colonies are always located on the same location accross the different runs.
    foreach (range 1 (nb-colonies + 1)) [ index ->
      ask one-of patches [
        set colony-index index
        set pcolor white
        set capacity 40
      ]
    ]
  ]

  ]
end

to initial-population
  create-cats initial-population-size
  ask cats [
    set sex random-sex
    cat-init ;; init cat variables
  ]
  ask cats [
    set age random-age
    set colony one-of (range 1 (nb-colonies + 1))
    move-to-colony
    set social-class random-social-class
  ]
end

to move-to-colony
  let target-colony colony
  move-to one-of patches with [ colony-index = target-colony ]
end


;; Create the initial state of a cat
;; cf. to birth
to cat-init
  set shape "wolf"
  set age 0
  set sexual-maturity-state false
  set sexual-maturity-date  cat-sexual-maturity-date
  set reproductive-control "none"
  set dominance-score 0
  set pregnancy-state "none"
  set pregnancy-date 0
  set copulation-count 0
end

to-report random-sex
  ifelse random 2 = 1
  [ report "F" ]
  [ report "M" ]
end

to-report random-social-class
  ifelse sex = "F"
  [
    let social-class-min 1
    let social-class-max colony-social-classes
    let random-value (random-within social-class-min social-class-max)
    report random-value
  ]
  [ report 0 ]
end

; > each cat is assigned an age with 20% probability of kitten,
; > 20% probability of juvenile, and 60% of adult
to-report random-age
  let age-category-sample random-float 1
  if age-category-sample < 0.20 ;; the cat is a kitten
  [
    let age-min 0
    let age-max JUVENILE_AGE_BEGIN
    report floor random-within age-min age-max
  ]
  if age-category-sample < 0.40 ;; the cat is a juvenile
  [
    let age-min JUVENILE_AGE_BEGIN
    let age-max ADULT_AGE_BEGIN
    report floor random-within age-min age-max
  ]
  ;; the cat is an adult
  ;; we consider an unrealistic uniform age distribution for adults too.
  let age-min ADULT_AGE_BEGIN
  let age-max CAT_LONGEVITY
  report floor random-within age-min age-max
end

;;
;; Update
;;

to go
  ;; Reset migrations reporter
  set migrations []

  ;; Update time
  set day day + 1
  if day >= 365
  [ set day 0
    set year year + 1
  ]
  ask cats [
    aging
    survival
  ]
  if day = 126 and year = 3 [
    ask cats [
      reproductive-control-programs
    ]
  ]
  ask cats with [ not sexual-maturity-state ] [
    maturation
  ]
  ask cats with [ sexual-maturity-state and sex = "M" ] [
    compute-dominance-score
    migration
  ]
  if in-mating-season [
    ask cats with [ sex = "F" and sexual-maturity-state and reproductive-control != "spayed" ] [
      if pregnancy-state = "none" and random-float 1 < estrus-enter-probability [
        begin-estrus
      ]
    ]
    ask cats with [ sex = "F" and sexual-maturity-state and reproductive-control != "spayed"] [
      update-estrus
    ]
    let colony-cats map neighbor-cats colonies
    foreach colony-cats [ cat-colony ->
      mating cat-colony
    ]
  ]
  ask cats with [ sex = "F" ] [
    pregnancy
  ]
  ask cats with [ sex = "F" ] [
    birth
  ]
  ask cats [
    cat-style
  ]
  tick
end

;; Report whether the date falls in the cats mating season
to-report in-mating-season
  ;; use Unix date command `date -d "1970-03-01" +"%j"` or `date -d "1970-10-01" +"%j"`
  let mating-season-start-day 60
  let mating-season-end-day 274
  report mating-season-start-day < day and day < mating-season-end-day
end

;;
;;  Cat behavior
;;

to aging
  set age age + 1
end

to survival
  if random-float 1 < 1 - daily-cat-survival-rate
  [ die ]
end

;; Juvenile cat may become mature
to maturation
  if age > sexual-maturity-date
  [ set sexual-maturity-state true ]
end

;; sexually mature and intact female enter estrus on March 1st of each year
to begin-estrus
  set pregnancy-state "estrus"
  set pregnancy-date 0
  set copulation-count 0
end

to update-estrus
  if pregnancy-state = "estrus" and pregnancy-date = ESTRUS_MAX_DURATION
  [
    set pregnancy-state "diestrus"
    set pregnancy-date 0
  ]
  if pregnancy-state = "diestrus" and pregnancy-date = DIESTRUS_DURATION
  [ begin-estrus ]
  if pregnancy-state = "estrus" or pregnancy-state = "diestrus"
  [ set pregnancy-date pregnancy-date + 1 ]
end

to mating [ colony-cats ]
  ;; list of all social classes that contain at least one female in estrus
  let classes-with-estrus [social-class] of colony-cats with [sex = "F" and sexual-maturity-state and pregnancy-state = "estrus"]

  ;; all non-neutered, sexually mature males are listed in order of dominance
  let available-males sort-on [dominance-score] colony-cats with [sex = "M" and reproductive-control != "neutered" and sexual-maturity-state]
  set available-males reverse available-males

  ;; male paired randomly with social class, copulate
  while [(not empty? classes-with-estrus) and (not empty? available-males)]
  [
    let male-head first available-males
    let class one-of classes-with-estrus
    let copulations random 10 + 1 ;; Random number between 1 and 10
    ask male-head [
      let male-reproductive-control reproductive-control
      repeat copulations [
        ;; Find a female in estrus from the selected class
        let female one-of (colony-cats with [social-class = class and sex = "F" and pregnancy-state = "estrus"])
        if female != nobody [
          ;; Copulate with the female
          ask female [
            evaluate-ovulation male-reproductive-control
          ]
        ]
      ]
    ]
    ;; male and social class removed from the lists
    set available-males remove-item 0 available-males
    set classes-with-estrus remove-item (position class classes-with-estrus) classes-with-estrus
  ]
end


to evaluate-ovulation [ male-reproductive-control ]

  ;; Increment copulation count for the female
  set copulation-count copulation-count + 1

  ;; Evaluate ovulation probability based on the number of copulations
  let ovulation-probability 0
  ifelse copulation-count = 1
  [ set ovulation-probability 0.2 ]
  [ ifelse copulation-count = 2
    [ set ovulation-probability 0.35 ]
    [ ifelse copulation-count = 3
      [ set ovulation-probability 0.5 ]
      [ set ovulation-probability 0.8 ]
    ]
  ]

  ;; Simulate ovulation
  if random-float 1 < ovulation-probability [
    ;; Ovulation occurs
    ifelse male-reproductive-control != "vasectomy" and reproductive-control != "hysterectomy" [
      set pregnancy-state "pregnant"
    ]
    [
      set pregnancy-state "pseudo-pregnant"
    ]
    set pregnancy-date 0
    set copulation-count 0
  ]
end

to pregnancy
  if pregnancy-state = "pseudo-pregnant" and pregnancy-date = PSEUDOPREGNANCY_DURATION [
    ifelse in-mating-season [
      set pregnancy-state "estrus"
    ][
      set pregnancy-state "none"
    ]
    set pregnancy-date 0
  ]
  if pregnancy-state = "nurse" and pregnancy-date = NURSE_DURATION [
    ifelse in-mating-season [
      set pregnancy-state "estrus"
    ][
      set pregnancy-state "none"
    ]
    set pregnancy-date 0
  ]
  if pregnancy-state = "pregnant" or pregnancy-state = "pseudo-pregnant" or pregnancy-state = "nurse"
  [ set pregnancy-date pregnancy-date + 1 ]
end


to birth
  if pregnancy-state = "pregnant" and pregnancy-date = PREGNANCY_DURATION
  [
    let inherited-social-class social-class
    hatch litter-size [
      set sex random-sex
      cat-init
      if sex = "F" [
        set social-class inherited-social-class
      ]
    ]
    set pregnancy-state "nurse"
  ]
end


;;  Litter size is either 3 or 4 kittens, with mean litter size being 3.2 kittens
to-report litter-size
  ifelse random-float 1 < 0.2
  [ report 4 ]
  [ report 3 ]
end

;; Dominance ranking
;; "At time of maturity, each male is given a dominance value of
;; zero. This value increases linearly from 0 at age 420 days to a value
;; of 1 at age 1080 days. All males of age 1080 days or older have
;; dominance 1.0" (Ireland et Miller Neilan, 2016).
to compute-dominance-score
  set dominance-score age - 420 / (1080 - 420)
end


;;
;; Colony dynamics
;;
to-report colonies
 report (range 1 (nb-colonies + 1))
end

;; Male cats change of colonies, not because of resource lack (food, shelter)
;; but rather in case they are too low in the dominance hierarchy to pretend
;; to mate with female cats of the colony

;; Daily probability of a male cat leaving his current colony (migration) is determined by its dominance value D
;; Prob_M = alpha (1 - D)^beta
;; alpha: max-migration-probability
;; beta: migration-dominance-coefficient
to-report migration-probability
  report max-migration-probability * (1 - dominance-score) ^ migration-dominance-coefficient
end

to migration
  if random-float 1 < migration-probability and nb-colonies > 1
  [

    let other-colonies filter [ c -> c != colony ] colonies
    let colony-weights colony-probabilities other-colonies
    let next-colony (weighted-choice other-colonies colony-weights)
    set migrations lput (list colony next-colony) migrations
    set colony next-colony
    move-to-colony
  ]
end

;; ref. https://stackoverflow.com/a/25369176/13518918
to-report weighted-choice [ values weights ]

  ; Calculate the cumulative probability list
  let cum reduce [ [ acc curr ] -> lput (curr + (ifelse-value (empty? acc) [0] [ last acc ])) acc ] (fput [] weights)

  ; Roll a uniform random number weighted by the cumulative probability vector
  let x random-float sum weights
  let j  -1
  let found false
  while [(not found) and (j < (length cum))]
  [
    set j (j + 1)
    if (x <= item j cum) [set found true]
  ]

  report item j values
end


;; Probability to join colony a given by Prob_R (a) = gamma / (n-1) (d_a) ^ m,
;; where n is the number of colonies, d_a is the distance from the current cat colony
;; to the colony a and gamma is defined as
;; gamma = (n-1) / ((d_1^m)^(-1) + (d_2^m)^(-1) + ... + (d_(n-1)^m)^(-1))
to-report colony-probabilities [ other-colonies ]
  let m colony-connectivity
  let colony-distances map colony-distance other-colonies
  let gamma_denominator sum map [ d -> 1 / (d ^ m) ] colony-distances
  let gamma (nb-colonies - 1) / (gamma_denominator)
  let probabilities map [ d -> gamma / ((nb-colonies - 1) * (d ^ m)) ] colony-distances
  report probabilities
end

to-report colony-distance [ index ]
  report distance one-of patches with [ colony-index = index ]
end

;;
;; Reproductive control
;;

to reproductive-control-programs
  if reproductive-control-program = "tnr" [
    trap-neuter-return
  ]
  if reproductive-control-program = "tvhr" [
    trap-hysterectomy-vasectomy-return
  ]
end

to trap-neuter-return
  if reproductive-control = "none" [
    if random-float 1 < program-probability
    [
      ifelse sex = "F"
      [ set reproductive-control "spayed" ]
      [
        ifelse sex = "M"
        [ set reproductive-control "neutered" ]
        [ error "Error: trap-neuter-return: cat sex is not in { F, M }" ]
      ]
    ]
    ;; pregnant females are not pregnant anymore
    set pregnancy-state "none"
    set pregnancy-date 0
  ]
end

to trap-hysterectomy-vasectomy-return
  if reproductive-control = "none" [
    if random-float 1 < program-probability
    [
      ifelse sex = "F"
      [ set reproductive-control "hysterectomy" ]
      [
        ifelse sex = "M"
        [ set reproductive-control "vasectomy" ]
        [ error "Error: trap-neuter-return: cat sex is not in { F, M }" ]
      ]
    ]
    ;; pregnant females are not pregnant anymore
    set pregnancy-state "none"
    set pregnancy-date 0
  ]
end

;;
;; Cat representation
;;

to cat-style
  set size cat-size
  set color cat-color
end

to-report cat-size
  let kitten-size 1
  let juvenile-size 2
  let adult-size 2.5
  ifelse age < JUVENILE_AGE_BEGIN
  [ report kitten-size ]
  [ ifelse age < ADULT_AGE_BEGIN
    [ report juvenile-size ]
    [ report adult-size ]
  ]
end

to-report cat-color
  let female-color brown
  let male-color orange
  let chosen-color grey
  ifelse sex = "F"
  [ set chosen-color female-color ]
  [ set chosen-color male-color ]
  ;;let age-mapped-color scale-color chosen-color age 0 50
  ;;report age-mapped-color
  report chosen-color
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
647
448
-1
-1
13.0
1
10
1
1
1
0
1
1
1
-16
16
-16
16
0
0
1
ticks
30.0

BUTTON
50
83
180
116
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
50
176
181
209
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
145
560
345
710
population
t
individuals
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"female" 1.0 0 -987046 true "" "plot count cats with [ sex = \"F\" ]"
"male" 1.0 0 -11881837 true "" "plot count cats with [ sex = \"M\" ]"
"all" 1.0 0 -7500403 true "" "plot count cats"

SLIDER
712
105
940
138
initial-population-size
initial-population-size
1
1000
292.0
1
1
NIL
HORIZONTAL

PLOT
378
566
578
716
age distribution
t
mean age
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot (sum [ age ] of cats) / (count cats)"

SLIDER
713
424
1034
457
program-probability
program-probability
0
1
0.99
0.01
1
NIL
HORIZONTAL

SLIDER
712
289
884
322
nb-colonies
nb-colonies
0
5
4.0
1
1
NIL
HORIZONTAL

SLIDER
712
245
975
278
migration-dominance-coefficient
migration-dominance-coefficient
1
10
2.0
1
1
NIL
HORIZONTAL

SLIDER
711
206
927
239
max-migration-probability
max-migration-probability
0
1
0.3
0.1
1
NIL
HORIZONTAL

SLIDER
715
535
887
568
colony-connectivity
colony-connectivity
0
5
0.5
0.5
1
NIL
HORIZONTAL

SWITCH
894
288
1083
321
fixed-colony-position
fixed-colony-position
0
1
-1000

SLIDER
713
162
922
195
estrus-enter-probability
estrus-enter-probability
0
1
1.0
0.01
1
NIL
HORIZONTAL

PLOT
674
616
874
766
pregnant
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count cats with [ sex = \"F\" and pregnancy-state = \"pregnant\" ]"

PLOT
891
616
1091
766
pseudo-pregnant
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count cats with [ sex =  \"F\" and pregnancy-state = \"pseudo-pregnant\" ]"

PLOT
1112
614
1312
764
nurse
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count cats with [ sex = \"F\" and pregnancy-state = \"nurse\" ]"

CHOOSER
713
375
930
420
reproductive-control-program
reproductive-control-program
"none" "tnr" "tvhr"
0

@#$#@#$#@
## WHAT IS IT?

This model tries to implement the ideas behind the paper [1] by Ireland and Miller Neilan, 2016.

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

[1] Ireland, T. and Miller Neilan, R. _A spatial agent-based model of feral cats and analysis of population and nuisance controls_, Ecological Modeling 337 (2016).


Our code for the model is available at <https://framagit.org/geniomhe/mosibio-2024>.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

cat
false
0
Line -7500403 true 285 240 210 240
Line -7500403 true 195 300 165 255
Line -7500403 true 15 240 90 240
Line -7500403 true 285 285 195 240
Line -7500403 true 105 300 135 255
Line -16777216 false 150 270 150 285
Line -16777216 false 15 75 15 120
Polygon -7500403 true true 300 15 285 30 255 30 225 75 195 60 255 15
Polygon -7500403 true true 285 135 210 135 180 150 180 45 285 90
Polygon -7500403 true true 120 45 120 210 180 210 180 45
Polygon -7500403 true true 180 195 165 300 240 285 255 225 285 195
Polygon -7500403 true true 180 225 195 285 165 300 150 300 150 255 165 225
Polygon -7500403 true true 195 195 195 165 225 150 255 135 285 135 285 195
Polygon -7500403 true true 15 135 90 135 120 150 120 45 15 90
Polygon -7500403 true true 120 195 135 300 60 285 45 225 15 195
Polygon -7500403 true true 120 225 105 285 135 300 150 300 150 255 135 225
Polygon -7500403 true true 105 195 105 165 75 150 45 135 15 135 15 195
Polygon -7500403 true true 285 120 270 90 285 15 300 15
Line -7500403 true 15 285 105 240
Polygon -7500403 true true 15 120 30 90 15 15 0 15
Polygon -7500403 true true 0 15 15 30 45 30 75 75 105 60 45 15
Line -16777216 false 164 262 209 262
Line -16777216 false 223 231 208 261
Line -16777216 false 136 262 91 262
Line -16777216 false 77 231 92 261

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.4.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="connectivity-experiment" repetitions="30" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>year &gt; 6</exitCondition>
    <metric>length filter [ m -&gt; m = [ 1 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 1 3 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 1 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 3 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 3 ]] migrations</metric>
    <enumeratedValueSet variable="initial-population-size">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="estrus-enter-probability">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproductive-control-program">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="migration-dominance-coefficient">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-colony-position">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-migration-probability">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="program-probability">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="colony-connectivity">
      <value value="0.25"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nb-colonies">
      <value value="4"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="population-dynamics" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>year &gt;= 6</exitCondition>
    <metric>count cats</metric>
    <metric>year</metric>
    <metric>day</metric>
    <enumeratedValueSet variable="initial-population-size">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="estrus-enter-probability">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproductive-control-program">
      <value value="&quot;none&quot;"/>
      <value value="&quot;tnr&quot;"/>
      <value value="&quot;tvhr&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="migration-dominance-coefficient">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-colony-position">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-migration-probability">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="program-probability">
      <value value="1"/>
      <value value="0.95"/>
      <value value="0.9"/>
      <value value="0.75"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="colony-connectivity">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nb-colonies">
      <value value="4"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="experiment" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>year &gt;= 6</exitCondition>
    <metric>length filter [ m -&gt; m = [ 1 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 1 3 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 1 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 3 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 2 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 3 4 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 1 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 2 ]] migrations</metric>
    <metric>length filter [ m -&gt; m = [ 4 3 ]] migrations</metric>
    <enumeratedValueSet variable="initial-population-size">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproductive-control-program">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="estrus-enter-probability">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="migration-dominance-coefficient">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-migration-probability">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="colony-connectivity">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-colony-position">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="program-probability">
      <value value="0.99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nb-colonies">
      <value value="4"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
