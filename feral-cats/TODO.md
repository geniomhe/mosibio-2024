# TODO

(in no particular order).

- [x] Initial cat birth
- [x] Cat maturity
- [x] Cat death
- [x] Cat style 
- [x] Male cats dominance relations
- [ ] Cat mating
- [ ] Female cat pregnancy and reproduction
- [x] Cat reproductive control
- [x] Cat colonies
  - [x] Definite cat colonies positions
 - [x] Male cat migration
- [x] Behavior space study
  - [ ] Colony connectivity
  
