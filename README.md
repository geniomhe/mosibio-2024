# MOSIBIO 2024

<p align="center">


[report](./doc/report/report.pdf)
●
[slides](./doc/slides/slides.pdf)
●
[model](./feral-cats/cats.nlogo)

</p>

Project in the Teaching Unit _Modeling and simulation of biological systems_ in Master 1 GENIOMHE.

## Feral cats

Our main project consists in modeling feral cat colonies and the impact of reproductive controls on their population dynamics.

We attempted to [implement](./feral-cats/cats.nlogo) the model described in Ireland and Miller Neilan [1].

A report for this project is available in [./doc/report/report.pdf](./doc/report/report.pdf).

## Ishida biochemical model
As an exercise before the beginning of the project, we tried to implement a biochemical model.
Ishida [biochemical model](./biochemical-modeling) is a discrete model of reaction diffusion like patterns on an hexagonal grid. We played a little bit with this model [2]. We wrote a naive NetLogo implementation, using turtles as the hexagonal cells. Even though the NetLogo model seems to be working: it is outrageously slow and does not render beautiful reaction diffusion pattern smoothly.

Trying to overcome these difficulties, we wrote a [Rust program](./biochemical-modeling/rust/ishida) to implement the Ishida model, using _nannou_ to render the graphics.

---


## Team members

- Naïa Périnelle 
- Samuel Ortion

## References


[1] Ireland, Timothy, and Rachael Miller Neilan. “A Spatial Agent-Based Model of Feral Cats and Analysis of Population and Nuisance Controls.” Ecological Modelling 337 (October 2016): 123–36. https://doi.org/10.1016/j.ecolmodel.2016.06.014.

[2] Ishida, Takeshi. “Emergence of Turing Patterns in a Simple Cellular Automata-Like Model via Exchange of Integer Values between Adjacent Cells.” Discrete Dynamics in Nature and Society 2020 (January 28, 2020): e2308074. https://doi.org/10.1155/2020/2308074.

