\documentclass{scrartcl}
\usepackage{sty/report}

\titlehead{M1 GENIOMHE}
\title{A spatial agent-based model of feral cats and analysis of population}
\subtitle{Modeling and Simulation of Biological System}
\author{Naïa Périnelle \and Samuel Ortion}
\date{2024-05-08}
\addbibresource{../resources/references.bib}
\begin{document}

\maketitle


\section{Feral cats and the impact of cats on biodiversity}

Cats have a huge impact on biodiversity.
Both domestic and feral cats hunt a wide range of species 
as reported by cat owners to the \href{https://www.chat-biodiversite.fr/les-resultats.html}{citizen science program \textit{Cat and Biodiversity}}
by the French society for the study and protection of mammals (SFEPM).
In some islands the cats introduced by humans cause severe threat to endemic species.

Feral cats are unowned domestic cats (\textit{Felis catus}) living outdoors. 
They reproduce freely in suburban or rural areas, and rely on predation to sustain themselves.

\section{Cat reproductive control methods}

Several methods can be used to limit cat population through a control of their reproduction.
Temporary methods, such as hormonal treatments are not well suited for feral cats. So, we focus on permanent methods: sterilizations.

There exist two kinds of sterilization programs: trap-neuter-return (TNR) or trap-vasectomy-hysterectomy-return (TVHR).
In TNR, cats are castrated: reproductive organs are removed. 
In TVHR, cats are sterilized through hysterectomy (uterus removal, for females) or vasectomy (section of the male vasa deferentia, stopping sperm from joining cat semen): this method does not alter cat hormonal system nor mating behaviour.

Both reproductive control programs require surgery: cats must be trapped before vet can perform the surgery.

\iffalse

\begin{figure}
    \centering
\includegraphics[width=0.5\pagewidth]{../media/Camembert_final-2_SFEPM_2023_en.png}
\caption[Cats have strong impact on a variety of small animals]{
    Cats have strong impact on a variety of small animals.
    
    Proportion of domestic cat preys as reported by citizen scientists
    to the program \textquote{Cat and biodiversity} from the French
    society for the study and protection of mammals (SFEPM). 
    
    Source: SFEPM, 2023. Species group names are translated from the original chart.}
    \label{fig:cat-prey-repartition}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.5\columnwidth]{../media/Feral_cat_1.jpeg}
    \caption[A feral cat, neutered in a trap-neuter-return program]{
        A feral cat, neutered in a trap-neuter-return program.

        Source: Brocken Inaglory - Own work,  \href{https://creativecommons.org/licenses/by-sa/3.0}{CC BY-SA 3.0}, via \href{https://commons.wikimedia.org/w/index.php?curid=2750156}{Wikimedia Commons}
    }
\end{figure}

\fi

\section{Modeling}

We tried to reproduce the NetLogo model presented in \cite{irelandSpatialAgentbasedModel2016}.
The following is a summary of the main characteristics of the model. 
We used NetLogo version 6.4. Our model is available online at \url{https://framagit.org/sortion/mosibio-2024/}.
The exact constant values we used come directly from the referenced paper.

\subsection{Cats}

Cats are either male or female. Aged from 0 to 42 days,
we consider them as kittens. From 42 to 180 days old,
we consider them as juveniles.

\subsection{Colonies}

Each cat is associated with a colony. In the model, the colonies are symbolized by a single localized patch. Each colony is associated with a specific carrying capacity, corresponding to the number of cats the colony environment has enough resources for them to survive.


\subsection{Survival rates}

The age categories impact the calculation of the survival rates of the cats:
young cats tend to be more likely to die than experienced adults.

Cat survival rate \(Prob_{S}\) is updated on a daily basis.
This rate depends on the sex and age of the cat, as well as
the carrying capacity \(K\) of the colony and its current size \(P\).
If the colony is close to the carrying capacity of the colony,
the cats (and particularly kittens) are more likely to die,
as in the case of a lack of preys in the environment due to a too high predation pressure of neighbor cats.
The reproductive control status of adult cats also has an influence
on the survival rate (sterilized cats being less likely to die).
\Cref{eqn:survival-rates} is used to compute the daily survival rate
\cite{irelandSpatialAgentbasedModel2016}.

\begin{equation}
  \label{eqn:survival-rates}
  Prob_{S} = S_{0} - \frac{\left( S_{0} - S_{K} \right)P}{K}
\end{equation}

\subsection{Reproduction}

Cats become sexually mature at an age randomly chosen at their birth. For females this age is uniformly distributed between 10 and 12 months old, for males it is uniformly distributed between 12 and 14 months. Sexually mature females have a status attribute indicates his actual condition: in estrus, diestrus, pregnancy or pseudo-pregnancy. Sexually mature males have a dominance score increasing linearly with age until 36 months.

Mating season occurs from day 60 to day 274 each year (March 1 - October 1).

Non-spayed sexually mature females enter in estrus at the beginning of the mating season. The estrus cycle lasts for 8 days maximum. After 8 days, the female enters in diestrus cycle for 5 days before returning in estrus. There is no cycle out of the mating season.

During mating season, each day, cats of a same colony mating between them. A non-neutered male from highest dominance can copulate \(c\) times, with females in estrus from a same social class.
Females are evaluated for ovulation after each copulation in the season, with probability:
\begin{itemize}
    \item 1 copulation in the season: 0.2 probabilities to ovulate
    \item 2 copulation: 0.35
    \item 3 copulation: 0.5
    \item 4 copulation: 0.8
\end{itemize}
If, after a copulation, a female ovulate, it can turn pregnant during 65 days or enter in pseudo-pregnancy for 45 days depending on whether the mates are intact or not. In our model, only the last male before ovulation is considered for the enter in pregnancy or pseudo-pregnancy, in contrary to the reference paper where all previous males having copulated with the female are considered.

At the end of the pregnancy, the female births 3 or 4 kittens, and nurses them for 42 days. After a pseudo-pregnancy or the nursing, if the season mating is not ending, female enter in estrus.


\subsection{Cat migration}

Male cats may move from one colony to another,
depending on its social status in its current colony 
and the distance to the other colony. 

The probability for a male cat to move from its colony $c_0$ to another colony $c_i$ is given by \cref{eqn:probability-migration} \cite{irelandSpatialAgentbasedModel2016}.

\begin{equation}
    \label{eqn:probability-migration}
    Prob_R(c_i) = \frac{\gamma}{(n-1) (d_i)^m}
\end{equation}

\begin{equation}
    \gamma = \frac{n-1}{(d_1^m)^{-1} + (d_2^m)^{-1} + ... + (d_{n-1}^m)^{-1}}
\end{equation}

\begin{equation}
    \sum_{i=1}^{n-1} Prob_R(c_i) = 1
\end{equation}


\section{Model parameter space study: Population dynamics under reproductive control constraints}

We tried to reproduce the figure 3 from Ireland and Miller Neil an paper. 
We use a fixed initial population size for each condition, and run a specific condition 10 times. 

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{../media/plots/cats-population-dynamics.pdf}
    \caption[Population dynamics plots]{Population sizes for 6 years for the 4 colonies. From the upper panel to the lower panel, the proportion of cats subject to a reproductive control program ranges from 50\% to 100\%. Curves represents the population counts, with either a trap-neuter-return program (tnr), a trap-hysterectomy-return program (tvhr) or no reproductive control program.}
    \label{fig:population-dynamics}
\end{figure}

NetLogo behavior space tool was used, with the following varying parameters:

\begin{itemize}
    \item Reproductive control program: trap-neuter-return, trap-hysterectomy-vasectomy-return or no reproductive control
    \item Probability of a capture: 1, 0.95, 0.90, 0.75, 0.5.
\end{itemize}

We ran the experiments on the fixed colony setup described in \cite{irelandSpatialAgentbasedModel2016}.
Briefly, 4 colonies are placed on a Y shape: a central colony with larger carrying capacity (40 cats) at coordinate $(0, 7)$, is in the center, and three other smaller colonies are placed on $(-5, 5$, $(5, 5)$, and $(0, 0)$ coordinates.
We chose to initialize the population with 100 cats randomly distributed between the colonies.

\Cref{fig:population-dynamics} depicts the evolution of the population size for 6 years of simulation, with or without a reproductive control event at the 126th day of the third year. This figure is an attempt to reproduce the figure 3 from \cite{irelandSpatialAgentbasedModel2016}, but without the abandonment of new cats in the colonies.

We also find that TVHR seems to be the most efficient method to reduce cat population, compared to TNR. As expected, a higher reproductive control pressure is associated with a stronger and more persistent population reduction.

%It seems however that there remains an error in our model as a TNR of 100\% of cats at the given date does not result in a complete stop of the reproduction of the cats: this is probably because the TNR on pregnant females does not result in the stop of the pregnancy.

\section{Conclusion}

We successfully implanted the main components of the model described in \cite{irelandSpatialAgentbasedModel2016}.
We found similar results, with the population dynamics plots. Trap Neuter Return and Trap Hysterectomy Vasectomy Return programs can be efficient to reduce cat population size, as long as the proportion of cat treated is large enough, and there is not much new abandon.


\printbibliography%

\end{document}
