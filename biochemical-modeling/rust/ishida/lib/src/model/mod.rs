
pub mod parameters;

pub mod world;
pub use world::World;

pub const EVEN_NEIGHBORHOOD_SHIFT: [[isize; 2]; 6] = [[0, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0]];
pub const ODD_NEIGHBORHOOD_SHIFT: [[isize; 2]; 6] = [[0, 1], [1, 1], [1, 0], [0, -1], [-1, 0], [-1, 1]];
