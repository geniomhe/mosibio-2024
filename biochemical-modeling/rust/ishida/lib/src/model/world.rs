use std::vec;

use rand::distributions::{Distribution, Uniform};
use rand::seq::SliceRandom;

use crate::model::EVEN_NEIGHBORHOOD_SHIFT;
use crate::model::ODD_NEIGHBORHOOD_SHIFT;

use super::parameters;

/// Remove all at indices
/// source: https://stackoverflow.com/a/57948703/13518918
pub fn remove_all<T>(source: &mut Vec<T>, indices_to_remove: &[usize]) -> Vec<T> {
    let mut indices = indices_to_remove.to_vec();
    indices.sort();
    indices.reverse();
    indices
        .iter()
        .copied()
        .map(|i| source.swap_remove(i))
        .collect()
}

pub struct World {
    pub cell_tokens: Vec<Vec<u64>>,
    pub previous_cell_tokens: Vec<Vec<u64>>,
    pub cell_states: Vec<bool>,
    pub width: usize,
    pub height: usize,
}

impl World {
    pub fn new(width: usize, height: usize) -> World {
        World {
            width: width,
            height: height,
            cell_tokens: vec![vec![]; width * height],
            previous_cell_tokens: vec![vec![]; width * height],
            cell_states: vec![false; width * height],
        }
    }

    pub fn boundary(&self, signed_i: isize, signed_j: isize) -> (usize, usize) {
        let i = (signed_i + self.height as isize) as usize % self.height;
        let j = (signed_j + self.width as isize) as usize % self.width;
        (i, j)
    }

    /// Hexagonal neighborhood
    ///
    /// i: row index
    /// j: column index
    pub fn neighbors(&self, i: usize, j: usize) -> Vec<(usize, usize)> {
        let mut coordinates: Vec<(usize, usize)> = Vec::new();
        let neighborhood_position_shift;
        if i % 2 == 0 {
            neighborhood_position_shift = EVEN_NEIGHBORHOOD_SHIFT;
        } else {
            neighborhood_position_shift = ODD_NEIGHBORHOOD_SHIFT;
        }
        for [xshift, yshift] in neighborhood_position_shift {
            let neighbor_i: isize = i as isize + yshift;
            let neighbor_j: isize = j as isize + xshift;
            let (neighbor_i, neighbor_j) = self.boundary(neighbor_i, neighbor_j);
            coordinates.push((neighbor_i, neighbor_j));
        }
        coordinates
    }

    pub fn init(&mut self) {
        let center = (self.width / 2 - 1, self.height / 2 - 1);
        self.cell_states[center.0 * self.width + center.1] = true;
    }

    pub fn update(&mut self) {
        self.previous_cell_tokens = self.cell_tokens.clone();
        self.token_generation();
        self.token_diffusion();
        self.token_dissipation();
        self.update_states();
    }

    pub fn token_generation(&mut self) {
        let n: usize = parameters::TOKEN_PRODUCTION;
        for i in 0..self.height {
            for j in 0..self.width {
                if self.cell_states[i * self.width + j] {
                    for _ in 0..n {
                        self.cell_tokens[i * self.width + j].push(1);
                    }
                }
            }
        }
    }

    pub fn generate_tokens(&mut self, i: usize, j: usize, n: usize) {
        let mut new_tokens: Vec<u64> = vec![1; n];
        let tokens = &mut self.cell_tokens[i * self.width + j];
        tokens.append(&mut new_tokens);
    }

    pub fn token_diffusion(&mut self) {
        for i in 0..self.height {
            for j in 0..self.width {
                self.diffuse_tokens(parameters::RESIDUAL_TOKEN_RATIO, i, j);
            }
        }
    }

    /// Diffuse tokens to neighbors
    pub fn diffuse_tokens(&mut self, r: f64, i: usize, j: usize) {
        if self.cell_tokens[i * self.width + j].is_empty() {
            return;
        }
        let neighbor_indices = self.neighbors(i, j);
        let mut rng = rand::thread_rng();
        let six_sided_die: Uniform<usize> = Uniform::from(0..6);
        let mut tokens = self.previous_cell_tokens[i * self.width + j].clone();
        let nb_tokens = tokens.len();
        let unchanged_tokens = r * nb_tokens as f64;
        let diffused_tokens = nb_tokens as f64 - unchanged_tokens;
        let returning_tokens: usize = (diffused_tokens * (1.0 / 6.0)) as usize;
        let diffusing_tokens: usize = diffused_tokens as usize - returning_tokens;
        let mut indices: Vec<usize> = (0..nb_tokens).collect();
        indices.shuffle(&mut rng);
        let diffusing_indices: &[usize] = &indices[..diffused_tokens as usize];
        // let returning_indices: &[usize] = &diffusing_indices[..returning_tokens];
        let moving_indices: &[usize] = &diffusing_indices[returning_tokens..];
        // Increment all diffusing token labels
        for diffusing_index in diffusing_indices {
            tokens[*diffusing_index] += 1;
        }
        self.cell_tokens[i * self.width + j] = tokens.clone();
        // Move diffusing tokens to randomly chosen neighbor cell
        for moving_index in moving_indices {
            let neighbor_index = six_sided_die.sample(&mut rng);
            // dbg!(neighbor_index);
            let neighbor = neighbor_indices[neighbor_index];
            // dbg!(neighbor);
            self.append_token(neighbor.0, neighbor.1, tokens[*moving_index]);
        }
        // Remove all moved tokens
        self.remove_tokens(i, j, diffusing_tokens);
        // Update tokens
        self.cell_tokens[i * self.width + j] = tokens;
    }

    pub fn get_state(&self, i: usize, j: usize) -> bool {
        self.cell_states[i * self.width + j]
    }

    pub fn append_token(&mut self, i: usize, j: usize, token: u64) {
        // dbg!(i, j, token);
        self.cell_tokens[i * self.width + j].push(token);
    }

    pub fn remove_tokens(&mut self, i: usize, j: usize, n: usize) {
        let mut tokens = &mut self.cell_tokens[i * self.width + j];
        let len = tokens.len();
        let new_tokens = remove_all::<u64>(tokens, &(len - n..len).collect::<Vec<usize>>());
        *tokens = new_tokens;
    }

    pub fn update_state(&mut self, i: usize, j: usize) {
        let age_threshold = parameters::MAX_LABEL / 2;
        let nb_young_tokens = self.cell_tokens[i * self.width + j]
            .iter()
            .filter(|&x| *x < age_threshold)
            .count();
        let nb_tokens = self.cell_tokens[i * self.width + j].len();
        let threshold = (parameters::AGE_RATE_THRESHOLD * nb_tokens as f64) as usize;
        match nb_young_tokens.cmp(&threshold) {
            std::cmp::Ordering::Greater => self.cell_states[i * self.width + j] = true,
            std::cmp::Ordering::Less => self.cell_states[i * self.width + j] = false,
            std::cmp::Ordering::Equal => (),
        }
    }

    pub fn update_states(&mut self) {
        for i in 0..self.height {
            for j in 0..self.width {
                self.update_state(i, j);
            }
        }
    }

    pub fn token_dissipation(&mut self) {
        for i in 0..self.height {
            for j in 0..self.width {
                let nb_tokens = self.cell_tokens[i * self.width + j].len();
                let threshold = (parameters::TOKEN_DISSIPATION_RATE * nb_tokens as f64) as usize;
                if nb_tokens < threshold {
                    self.cell_tokens[i * self.width + j] = vec![];
                }
            }
        }
    }
}
