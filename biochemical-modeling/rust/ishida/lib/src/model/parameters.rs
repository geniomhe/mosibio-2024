pub const TOKEN_PRODUCTION: usize = 100; // b
pub const RESIDUAL_TOKEN_RATIO: f64 = 0.06; // r
pub const MAX_LABEL: u64 = 100; // X
pub const TOKEN_DISSIPATION_RATE: f64 = 0.1; // d
pub const AGE_RATE_THRESHOLD: f64 = 0.02; // w