/// # Takeshi Ishida model

const GENERATIONS: usize = 15;
const WORLD_SIZE: usize = 100;

use std::thread;

use indicatif::ProgressBar;

fn main() {
	let mut world = libishida::model::World::new(WORLD_SIZE,WORLD_SIZE);
	world.init();	
	// Create a buffer of world states
	let mut buffer: Vec<Vec<bool>> = Vec::new();
	buffer.push(world.cell_states.clone());
	let progress = ProgressBar::new(GENERATIONS as u64);
	for _ in 0..GENERATIONS {
		world.update();
		let states = world.cell_states.clone();
		buffer.push(states);
		progress.inc(1);
	}
	progress.finish_with_message("simulation finished");	
}