
pub const WORLD_SIZE: usize = 100;
pub const WINDOW_SIZE: u32 = 512;
// pub const CELL_SIZE: f64 = 10.0;
pub const CELL_SIZE: f32 = WINDOW_SIZE as f32 / WORLD_SIZE as f32 / 2.0;

pub const GENERATIONS: usize = 10;