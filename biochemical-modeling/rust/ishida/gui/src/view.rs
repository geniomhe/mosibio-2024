
use nannou::prelude::*;
use libishida::model;

use crate::config;


pub fn get_color(state: bool) -> Rgb<u8>  {
    if state {
        BLACK
    } else {
        WHITE
    }
}

/// ref. https://github.com/PistonDevelopers/piston-examples/blob/master/examples/shapes.rs
pub fn draw_hexagon(x: f32, y: f32, color: Rgb<u8>, draw: &Draw) {
    // Translate the hexagon to the (x, y) position
    let points = (0..=360).step_by(60).map(|i| {
        let radian = deg_to_rad(i as f32);
        let shape_x = radian.sin() * config::CELL_SIZE as f32;
        let shape_y = radian.cos() * config::CELL_SIZE as f32;
        let x = x + shape_x;
        let y = y + shape_y;
        pt2(x, y)
    });
    draw.polygon().color(color).points(points);
}

fn scale_coordinate(coordinate: f32, max_coordinate: f32, max_window: f32) -> f32 {
    coordinate / max_coordinate * max_window + config::CELL_SIZE - config::WINDOW_SIZE as f32 / 2.0
}

pub fn draw_world(world: &model::World, draw: &Draw) {
    draw_world_with_states(world, &world.cell_states.clone(), draw);
}

pub fn draw_world_with_states(world: &model::World, states: &Vec<bool>, draw: &Draw) {
    for i in 0..world.height {
        for j in 0..world.width {
            let mut shifted_i = i as f32;
            if j % 2 == 0 {
                shifted_i -= 0.5;
            }
            let state = states[i * world.width + j];
            let y = scale_coordinate(shifted_i, world.height as f32, config::WINDOW_SIZE as f32);
            let x = scale_coordinate(j as f32, world.width as f32, config::WINDOW_SIZE as f32);
            let color = get_color(state);
            draw_hexagon(x, y, color, draw);
        }
    }
}

pub fn draw_neighbors(world: &model::World, i: usize, j: usize, draw: &Draw) {
    let neighborhood_position_shift= if i % 2 == 0 {
        model::EVEN_NEIGHBORHOOD_SHIFT
    } else {
        model::ODD_NEIGHBORHOOD_SHIFT
    };
    for [xshift, yshift] in &neighborhood_position_shift {
        let neighbor_i = ((i as isize + yshift) % world.height as isize) as usize;
        let neighbor_j = ((j as isize + xshift) % world.width as isize) as usize;
        let mut shifted_i = neighbor_i as f32;
        if neighbor_j % 2 == 0 {
            shifted_i -= 0.5;
        }
        let color = RED;
        let y = scale_coordinate(shifted_i, world.height as f32, config::WINDOW_SIZE as f32);
        let x = scale_coordinate(neighbor_j as f32, world.width as f32, config::WINDOW_SIZE as f32);
        draw_hexagon(x, y, color, draw);
    }
}
