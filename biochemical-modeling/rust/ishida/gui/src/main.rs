/// # Takeshi Ishida model
/// 
use std::thread;
use nannou::prelude::*;

use indicatif::ProgressBar;

#[path="config.rs"]
mod config;

mod view;

struct Model {
    buffer: Vec<Vec<bool>>,
    world: libishida::model::world::World,
    iteration: usize,
}


fn model(_app: &App) -> Model {
	let mut world = libishida::model::World::new(config::WORLD_SIZE, config::WORLD_SIZE);
	world.init();	
	// Create a buffer of world states
	let mut buffer: Vec<Vec<bool>> = Vec::new();
	buffer.push(world.cell_states.clone());
	let progress = ProgressBar::new(config::GENERATIONS as u64);
	for _ in 0..config::GENERATIONS {
		world.update();
		let states = world.cell_states.clone();
		buffer.push(states);
		progress.inc(1);
	}
	progress.finish_with_message("simulation finished");
	
	Model { buffer: buffer, world: world, iteration: 0}
}

fn update(_app: &App, model: &mut Model, _update: Update) {
	model.iteration = (model.iteration + 1) % config::GENERATIONS;
}

fn main() {
	nannou::app(model).update(update).simple_window(view).size(config::WINDOW_SIZE, config::WINDOW_SIZE).run();
}

fn view(app: &App, model: &Model, frame: Frame) {
	let draw = app.draw();
	let buffer = &model.buffer;
	let states = &buffer[model.iteration];
	thread::sleep(std::time::Duration::from_millis(1000));
	view::draw_world_with_states(&model.world, states, &draw);
	draw.to_frame(app, &frame).unwrap();
}