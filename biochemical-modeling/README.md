# Biochemical modeling with NetLogo

[Hexagonal neighboring](./media/hexagonal-neighborhood_ishida_rust.png)

## References

- Takeshi Ishida. "Emergence of Turing Patterns in a Simple Cellular Automata-Like Model via Exchange of Integer Values between Adjacent Cells", in Discrete Dynamics in Nature and Society, 2020(4):1-12. <https://doi.org/10.1155/2020/2308074>
